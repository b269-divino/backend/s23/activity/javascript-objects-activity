let trainer = {
	name: 'Ash Ketchum',
	age: '10',
	pokemon: ['snorlax', 'charizardDragon', 'onyxSteelix','ninetails'],
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},
	talk: function(){
		console.log("Hello my name is " + this.name + ' ' + this.age);
	}
}
console.log(trainer);


function pokemonTrainer(name, pokemon) {
	//"This" keyword allows us to get the value of the information
	this.name = name;
	this.pokemon = pokemon;
};

let pokemonOwner = new pokemonTrainer('Ash Ketchum');
console.log("Result from dot notation: ");
console.log(pokemonOwner.name);


function Pokemons(name, level, health, attack) {
	//"This" keyword allows us to get the value of the information
	this.name = name;
	// this.pokemon2 = pokemon2;
	// this.pokemon3 = onyxSteelix;
	// this.pokemon4 = ninetails;
};

let pokemonGen = new Pokemons('onyxSteelix','ninetails');

let pokemonGen1 = new Pokemons('snorlax', 'charizardDragon');

let array = [pokemonGen, pokemonGen1];
console.log(array);



// Objects


let pokeMaster = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ['snorlax', 'charizardDragon', 'onyxSteelix','ninetails'],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function() {
        console.log("CharizardDragon! I choose you!");
    }
};

console.log(pokeMaster)
console.log("Result of dot notation:")
console.log(pokeMaster.name)
console.log("Result of square bracket notation:")
console.log(pokeMaster.pokemon)
console.log('Result of talk method')
console.log(pokeMaster.talk())


function Pokemon(name, level, health, attack) {
    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * health;
    this.attack = attack;

    // Method
    this.tackle = function(opponent) {
        let remainingHealth = opponent.health - this.attack;
        console.log(this.name + ' tackled ' + opponent.name);
        console.log(opponent.name + "'s health is now reduced to " + remainingHealth);
        if (remainingHealth <= 0) {
            console.log(opponent.name + ' fainted.');
            opponent.level = 1; // 
            opponent.health = 0; // 
        } else {
            opponent.health = remainingHealth;
        }
    };
}


let pikachu = new Pokemon('snorlax', 12, 100, 12);
let geodude = new Pokemon('charizardDragon', 8, 80, 8);
let mewtwo = new Pokemon('onyxSteelix', 100, 200, 200  );
let rattata = new Pokemon('ninetails', 8, 80, 13 );




console.log (snorlax);
console.log (charizardDragon);
console.log (ninetails);


snorlax.tackle(charizardDragon)
console.log(charizardDragon);

ninetails.tackle(snorlax)
console.log(snorlax)
